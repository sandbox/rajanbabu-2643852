
jQuery.noConflict();
		jQuery(document).ready(function($){
      $(window).load(function() {

				$(".flexslider").fadeIn("slow");

				$(".flexslider").flexslider({
				animationLoop: true,  					
				animation: drupalSettings.effect,
				animationSpeed: drupalSettings.animation_time,
				controlNav: drupalSettings.slideshow_controls,
				directionNav: false,
				touch: drupalSettings.slideshow_touch,
				pauseOnHover: drupalSettings.slideshow_pause,
				slideshowSpeed: drupalSettings.effect_time,
				randomize: drupalSettings.slideshow_random,
				controlsContainer: "#slideshow"
				});
			});
		});  /* JQUERY ENDS */

