jQuery(document).ready(function($) {

  $("#navigation .content > ul").mobileMenu({
	prependTo: "#navigation",
	combine: false,
    switchWidth: drupalSettings.switch_width,
    topOptionText: drupalSettings.top_option_text
	});

});

